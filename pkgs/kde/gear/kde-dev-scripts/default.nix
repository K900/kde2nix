{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "kde-dev-scripts";

  meta.license = null;
}
