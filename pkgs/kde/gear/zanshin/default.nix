{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "zanshin";

  meta.license = null;
  # FIXME(signond)
  meta.broken = true;
}
