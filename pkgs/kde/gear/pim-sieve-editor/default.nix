{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "pim-sieve-editor";

  meta.license = null;
  # FIXME(signond)
  meta.broken = true;
}
