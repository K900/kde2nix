{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "grantlee-editor";

  meta.license = null;
  # FIXME(signond)
  meta.broken = true;
}
