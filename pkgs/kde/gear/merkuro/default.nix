{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "merkuro";

  meta.license = null;
  # FIXME(signond)
  meta.broken = true;
}
