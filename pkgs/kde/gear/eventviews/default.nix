{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "eventviews";

  meta.license = null;
  # FIXME(signond)
  meta.broken = true;
}
