{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "kdesdk-thumbnailers";

  meta.license = null;
}
