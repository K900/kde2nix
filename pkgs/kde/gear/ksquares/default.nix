{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "ksquares";

  meta.license = null;
}
