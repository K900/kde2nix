{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "incidenceeditor";

  meta.license = null;
  # FIXME(signond)
  meta.broken = true;
}
