{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "kio-zeroconf";

  meta.license = null;
}
