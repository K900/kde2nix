{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "plasma-disks";

  meta.license = null;
}
