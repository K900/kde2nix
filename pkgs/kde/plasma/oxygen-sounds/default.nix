{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "oxygen-sounds";

  meta.license = null;
}
