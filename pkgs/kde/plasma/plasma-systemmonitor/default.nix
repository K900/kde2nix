{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "plasma-systemmonitor";

  meta.license = null;
}
