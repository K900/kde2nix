{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "ocean-sound-theme";

  meta.license = null;
}
