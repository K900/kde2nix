{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "plasma-thunderbolt";

  meta.license = null;
}
