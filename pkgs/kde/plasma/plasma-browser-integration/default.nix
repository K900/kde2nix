{
  lib,
  mkKdeDerivation,
}:
mkKdeDerivation {
  pname = "plasma-browser-integration";

  meta.license = null;
}
